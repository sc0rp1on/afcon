-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 14, 2018 at 06:48 PM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teamscor_afcon`
--

-- --------------------------------------------------------

--
-- Table structure for table `tournament`
--

CREATE TABLE `tournament` (
  `id` int(11) NOT NULL,
  `host` varchar(15) NOT NULL,
  `lat` varchar(10) NOT NULL,
  `lng` varchar(10) NOT NULL,
  `year` int(4) NOT NULL,
  `winner` int(2) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tournament`
--

INSERT INTO `tournament` (`id`, `host`, `lat`, `lng`, `year`, `winner`, `description`) VALUES
(1, 'Egypt', '26.8206', '30.8025', 2006, 5, 'The 2006 Africa Cup of Nations was the 25th edition of the Africa Cup of Nations, the association football championship of Africa. It was hosted by Egypt. Just like in 2004, the field of sixteen teams was split into four groups of four. Egypt won its fifth championship, beating Cote d\'Ivoire in the final 4−2 in a penalty shootout after a goalless draw.'),
(2, 'Ghana', '7.9465', '1.0232', 2008, 5, 'The 2008 Africa Cup of Nations, also known as the MTN Africa Cup of Nations due to the competition\'s sponsorship by MTN, was the 26th edition of the Africa Cup of Nations, the biennial football tournament for nations affiliated to the Confederation of African Football (CAF). The tournament was staged at four venues around Ghana between 20 January and 10 February 2008. Egypt won the tournament, beating Cameroon.'),
(3, 'Angola', '-11.2027', '17.8739', 2010, 5, 'The 2010 Africa Cup of Nations, also known as the Orange Africa Cup of Nations for sponsorship reasons, was the 27th Africa Cup of Nations, the biennial football championship of Africa (CAF). It was held in Angola, where it began on 10 January 2010 and concluded on 31 January. In the tournament, the hosts Angola were to be joined by 15 nations who advanced from the qualification process that began in October 2007 and involved 53 African national teams.'),
(4, 'South Africa', '-26.2041', '28.0473', 2013, 15, 'The 2013 Africa Cup of Nations, also known as the Orange Africa Cup of Nations South Africa 2013 for sponsorship reasons, held from 19 January to 10 February 2013, was the 29th Africa Cup of Nations, the football championship of Africa organized by the Confederation of African Football (CAF). Starting from this edition, the tournament was switched to being held in odd-numbered years instead of even-numbered years so that it does not clash with the FIFA World Cup. South Africa hosted the tournament for the second time, after previously hosting the 1996 African Cup of Nations.'),
(5, 'Equitorial-Guin', '1.6508', '10.2679', 2015, 14, 'The 2015 Africa Cup of Nations, known as the Orange Africa Cup of Nations, Equatorial Guinea 2015 for sponsorship reasons, was the 30th staging of the Africa Cup of Nations, the international football championship of Africa. It was organized by the Confederation of African Football (CAF) and was held from 17 January to 8 February 2015. The tournament was initially scheduled to be hosted by Morocco who later demanded postponement of the event because of the Ebola virus epidemic in West Africa; subsequently Morocco was ruled out as a host country and replaced by Equatorial Guinea.'),
(6, 'Gabon', '0.8037', '11.6094', 2017, 4, 'The 2017 Africa Cup of Nations, known as the Total Africa Cup of Nations, Gabon 2017 (also referred to as AFCON 2017 or CAN 2017), was the 31st edition of the Africa Cup of Nations, the biennial international men\'s football championship of Africa organized by the Confederation of African Football (CAF). The tournament was scheduled to be hosted by Libya, until CAF rescinded its hosting rights in August 2014 due to ongoing war in the country. The tournament was instead hosted by Gabon.'),
(8, 'Cameroon', '7.3697', '12.3547', 2019, 16, 'The 2019 Africa Cup of Nations, known as the Total 2019 Africa Cup of Nations, is scheduled to be the 32nd edition of the Africa Cup of Nations, the biennial international men\'s football championship of Africa organized by the Confederation of African Football (CAF). The tournament is scheduled to be hosted by Cameroon. The competition will be held in June and July 2019, as per the decision of the CAF Executive Committee on 20 July 2017 to move the Africa Cup of Nations from January/February to June/July for the first time. It will also be the first Africa Cup of Nations expanded from 16 to 24 teams.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tournament`
--
ALTER TABLE `tournament`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk` (`winner`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tournament`
--
ALTER TABLE `tournament`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tournament`
--
ALTER TABLE `tournament`
  ADD CONSTRAINT `fk` FOREIGN KEY (`winner`) REFERENCES `teams` (`team_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
