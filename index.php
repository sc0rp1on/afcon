<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
        <link rel="manifest" href="images/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <title>AFCON - Skylark Web-Dev Assignment</title>
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.3/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin=""/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <div id="loader"></div>
        <div class="content">
            <nav class="navbar navbar-expand-lg navbar-dark nav">
                <a class="navbar-brand" href="/"><img class="fa fa-spin" src="images/logo.png" alt="logo"/><span class="mt-2">&nbsp;AFCON</span></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown" id="hosts-main">
                            <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#">Host Countries <span class="caret"></span></a>
                            <ul class="dropdown-menu" style="width: 220px;">
                                <div class="text-center py-2"><span class="text-danger">Hosts</span></div>
                                <div id="hosts">
                                    <li class="list-group-item text-center text-muted"><i class="fa fa-spin fa-spinner"></i></li>
                                </div>
                            </ul>
                        </li>
                        <li class="nav-item dropdown" id="teams-main">
                            <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#">Teams <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <div class="text-center py-2"><span class="text-danger">Participant Teams</span></div>
                                <div id="teams">
                                    <li class="list-group-item text-center text-muted"><i class="fa fa-spin fa-spinner"></i></li>
                                </div>
                            </ul>
                        </li>
                        <li class="nav-item dropdown" id="winners-main">
                            <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#">Winners <span class="caret"></span></a>
                            <ul class="dropdown-menu" style="width: 200px;">
                                <div class="text-center py-2"><span class="text-danger">Winner Teams</span></div>
                                <div id="winners">
                                    <li class="list-group-item text-center text-muted"><i class="fa fa-spin fa-spinner"></i></li>
                                </div>
                            </ul>
                        </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0 mr-4">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="row m-5 rounded row-main">
                    <div class="col-md-6">
                        <div id="map"></div>
                    </div>
                    <div class="col-md-6 details">
                        <h4 class="text-muted text-center p-2 mt-2" style="font-family: georgia;"><i class="fa fa-trophy"></i>&nbsp; African Cup of Nations &nbsp;<i class="fa fa-trophy"></i></h4>
                        <div class="table-responsive">
                            <table class="table table-striped text-left">
                                <tr>
                                    <td class="pl-5">
                                        <i class="fa fa-globe fa-custom"></i>&nbsp;
                                        <span class="text-muted" id="host">Host Country</span>
                                    </td>
                                    <td class="">
                                        <i class="fa fa-map-marker fa-custom"></i>&nbsp;
                                        <span class="text-muted" id="location">Location</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pl-5">
                                        <i class="fa fa-shield fa-custom"></i>&nbsp;&nbsp;
                                        <span class="text-muted" id="winner">Winner</span>
                                    </td>
                                    <td class="">
                                        <i class="fa fa-calendar-check-o fa-custom"></i>&nbsp;
                                        <span class="text-muted" id="year">Year</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <hr>
                        <div class="mb-3 mt-4">
                            <span class="p-2 px-3 mt-3 text-center desc">
                                <strong>Description: </strong><strong class="pull-right text-danger pr-3" id="support"></strong>
                            </span>
                        </div>
                        <p class="text-justify px-3" id="desc" style="font-size: 15px;">
                            The Total Africa Cup of Nations, officially CAN (French: Coupe d'Afrique des Nations), also referred to as African Cup of Nations, or AFCON, is the main international association football competition in Africa. It is sanctioned by the Confederation of African Football (CAF) and was first held in 1957. Since 1968, it has been held every two years. The title holders at the time of a FIFA Confederations Cup qualify for that competition.
                            <br/><br/>
                            In 1957 there were only three participating nations: Egypt, Sudan and Ethiopia. South Africa was originally scheduled to compete, but were disqualified due to the apartheid policies of the government then in power. Since then, the tournament has grown greatly, making it necessary to hold a qualifying tournament.
                        </p>
                        <!--<div class="text-center" id="support"></div>-->
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <footer>
                <!-- Copyright --> 
                <div class="text-center py-3 text-white"> 
                    <small>
                        Copyright © 2018 : <span class="text-danger">AFCON-SKYLARK</span>
                        <br/>
                        All rights reserved
                    </small>
                </div>
                <!-- Copyright -->
            </footer>
            <!-- Footer -->
        </div>
        <!-- The Modal -->
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content" style="background-color: ghostwhite;">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Choose Your Favorite Team</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body row"></div>
                    <!--                     Modal footer 
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        </div>-->
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://unpkg.com/leaflet@1.3.3/dist/leaflet.js" integrity="sha512-tAGcCfR4Sc5ZP5ZoVz0quoZDYX5aCtEm/eu1KhSLj2c9eFrylXZknQYmxUssFaVJKvvc0dJQixhGjG2yXWiV9Q==" crossorigin=""></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
        <script src="js/functions.js"></script>
    </body>
</html>