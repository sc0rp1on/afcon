<?php

include "functions.php";

if($_GET) {
    $choice = htmlspecialchars($_GET["choice"]);
    switch($choice) {
        case 1: echo getTournaments();
            break;
        case 2: 
            $lat = htmlspecialchars($_GET["lat"]);
            $lng = htmlspecialchars($_GET["lng"]);
            echo getDetails($lat, $lng);
            break;
        case 3: echo getTeams();
            break;
        case 4: echo getHosts();
            break;
        case 5: echo getWinners();
            break;
    }
}