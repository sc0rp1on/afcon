$(document).ready(function () {
    $("#loader").hide();
    $(".content").show();

    var mymap = L.map('map').setView([1.7832, 20.5085], 3);

    // L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYWRpc2luZ2gxOTkyIiwiYSI6ImNqa3RqOXlnODA2MnUzdnFuaWFhOWp2ZXoifQ.yZ07Di-HcASn0rDKXQQtww', {
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 10,
        minZoom: 3,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(mymap);

    var popup = L.popup();
    function onMapClick(e) {
//                mymap.flyTo({lat: 33.8869, lng: 9.5375});

        popup
                .setLatLng(e.latlng)
                .setContent("You clicked the map at " + e.latlng.toString())
                .openOn(mymap);
    }
    mymap.on('click', onMapClick);

    $.get("process.php?choice=1", function (data, status) {
        if (status === "success" && data) {
            var marks = JSON.parse(data);
            marks.forEach(function (element) {
                L.marker([element.lat, element.lng]).on("click", fetchDetails).addTo(mymap);
            });
        }
    });

    $("#teams-main").on("click", function () {
        $.get("process.php?choice=3", function (data, status) {
            if (status === "success" && data) {
                var teams = JSON.parse(data);
                var temp = "";
                teams.forEach(function (element) {
                    temp += '<li class="mx-1 px-3 py-1 text-muted border-bottom">' + element.team + '</li>';
                });
                $("#teams").html(temp);
            }
        });
    });

    $("#hosts-main").on("click", function () {
        $.get("process.php?choice=4", function (data, status) {
            if (status === "success" && data) {
                var hosts = JSON.parse(data);
                var temp = "";
                hosts.forEach(function (element) {
                    temp += '<li class="mx-1 px-3 py-1 text-muted border-bottom"><span>' + element.host + '</span> - <strong>' + element.year + '</strong></li>';
                });
                $("#hosts").html(temp);
            }
        });
    });

    $("#winners-main").on("click", function () {
        $.get("process.php?choice=5", function (data, status) {
            if (status === "success" && data) {
                var winners = JSON.parse(data);
                var temp = "";
                winners.forEach(function (element) {
                    temp += '<li class="mx-1 px-3 py-1 text-muted border-bottom"><span>' + element.team + '</span> - <strong>' + element.year + '</strong></li>';
                });
                $("#winners").html(temp);
            }
        });
    });

    $("#support").on("click", "#team-modal", function () {
        $.get("process.php?choice=3", function (data, status) {
            if (status === "success" && data) {
                var teams = JSON.parse(data);
                var temp = "";
                teams.forEach(function (element) {
                    temp += '<div class="col-md-3"><span class="badge m-2">' + element.team + '</span></div>';
                });
                $(".modal-body").html(temp);
            }
        });
        $('#myModal').modal('show');
    });

    $(".modal-body").on("click", ".badge", function () {
        cookieSetter(this.innerHTML);
    });
});

function fetchDetails(e) {
    var str = 'process.php?choice=2&lat=' + e.latlng.lat + '&lng=' + e.latlng.lng;
    $.get(str, function (data, status) {
        if (status === "success" && data) {
            var details = JSON.parse(data);
            $("#host").text(details.host);
            $("#location").text(details.lat + " " + details.lng);
            $("#year").text(details.year);
            $("#winner").text(details.team);
            $("#desc").text(details.description);
            support();
        }
    });
}

function support() {
    var year = $("#year").text();
    if (Cookies.get(year) === undefined) {
        $("#support").html('<small style="cursor: pointer;" id="team-modal">Which Team Did You Support?</small>');
    } else {
        tempYear = Cookies.get(year);
        $("#support").html('<small class="text-success">You chose ' + tempYear + ' as your favorite!</small>');
    }
}

function cookieSetter(team) {
    var year = $("#year").text();
    Cookies.set(year, team, {expires: 7});
    location.reload();
}
