# afcon

African Cup of Nations

A PoC(Proof of Concept) project demonstrating the mapping of various data points (markers) on a map using Leaflet & Mapbox
and fetching relevant information about them on request by user.

The project uses Javascript to generate markers on the map and PHP, MySQL is used at backend to provide relevant information for a particular point as requested by user.