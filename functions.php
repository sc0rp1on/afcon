<?php

function getConnection() {
    $servername = "localhost";
    $username = "teamscor_afcon";
    $password = "alec177fig600";
    $dbname = "teamscor_afcon";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    return $conn;
}

function getTournaments() {
    $conn = getConnection();
    $markers = array();

    // prepare sql statements
    $sql = "SELECT lat, lng FROM tournament";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($markers, $row);
        }
    }
    return json_encode($markers);
}

function getDetails($lat, $lng) {
    $conn = getConnection();
    $sql = "SELECT host, lat, lng, year, description, team FROM tournament INNER JOIN teams ON tournament.winner = teams.team_id WHERE lat=$lat AND lng=$lng;";
    $details = $conn->query($sql)->fetch_assoc();

    return json_encode($details);
}

function getTeams() {
    $conn = getConnection();
    $teams = array();

    $sql = "SELECT team FROM teams WHERE team != 'TBD';";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($teams, $row);
        }
    }
    return json_encode($teams);
}

function getHosts() {
    $conn = getConnection();
    $hosts = array();

    $sql = "SELECT host, year FROM tournament;";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($hosts, $row);
        }
    }
    return json_encode($hosts);
}

function getWinners() {
    $conn = getConnection();
    $hosts = array();

    $sql = "SELECT team, year FROM tournament INNER JOIN teams ON tournament.winner = teams.team_id;";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($hosts, $row);
        }
    }
    return json_encode($hosts);
}